# Vite Spring Boot Application

Welcome to the Vite Spring Boot Application! This project is built with Spring Boot and showcases some cool features.

## Prerequisites

Before you proceed, make sure you have the following installed on your machine:

- Java 17 or later
- Gradle
- Docker Compose

## Setup

1. Clone this repository to your local machine: git clone https://gitlab.com/NicatQuliyev/vite-app.git

2. Open a terminal or command prompt and navigate to the project's root directory.

3. Build the project using Gradle: ./gradlew build

4. Start the required services (MySQL) using Docker Compose: docker compose up

## How to Start and Run the Application

1. Make sure the Docker Compose services (MySQL) are running.

2. Run the application using Gradle with Java 17: ./gradlew bootRun

3. Once the application starts, open postman or swagger and register: `http://localhost:9090/api/auth/singup` 

4. When you registered you should get jwt token. Use this in Authorization for other requests

5. To stop the application, press `Ctrl+C` in the terminal or command prompt where the application is running.

## Additional Configuration

- If you want to change the default port (9090), you can modify the `application.yml` file located in `src/main/resources` and set the desired port.

- The application is already configured to use the MySQL instance provided by Docker Compose. You can find the connection properties in the `application.yml` file.

## Troubleshooting

- If you encounter any issues during the setup or execution of the application, please don't hesitate to [open an issue](https://gitlab.com/NicatQuliyev/vite-app/issues) on our GitLab repository.





