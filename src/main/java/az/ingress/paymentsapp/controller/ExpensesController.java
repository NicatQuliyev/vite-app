package az.ingress.paymentsapp.controller;

import az.ingress.paymentsapp.dto.request.ExpensesRequest;
import az.ingress.paymentsapp.dto.response.ExpensesResponse;
import az.ingress.paymentsapp.service.ExpensesService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping("/api/v1/expenses")
@RequiredArgsConstructor
@SecurityRequirement(name = "bearerAuth")
public class ExpensesController {

    private final ExpensesService expensesService;

    @GetMapping
    public ResponseEntity<List<ExpensesResponse>> findAll() {
        return new ResponseEntity<>(expensesService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/{expensesId}")
    public ResponseEntity<ExpensesResponse> findById(@PathVariable Long expensesId) {
        return new ResponseEntity<>(expensesService.findById(expensesId), HttpStatus.OK);
    }


    @PostMapping
    public ResponseEntity<ExpensesResponse> save(@ModelAttribute ExpensesRequest expensesRequest, @RequestParam("file") MultipartFile file) {
        return new ResponseEntity<>(expensesService.save(expensesRequest, file), HttpStatus.OK);
    }

    @PutMapping("/{expensesId}")
    public ResponseEntity<ExpensesResponse> update(@RequestBody ExpensesRequest request,
                                                   @PathVariable Long expensesId) {
        return new ResponseEntity<>(expensesService.update(expensesId, request), HttpStatus.OK);
    }

    @DeleteMapping("/{expensesId}")
    public void delete(@PathVariable Long expensesId) {
        expensesService.delete(expensesId);
    }
}
