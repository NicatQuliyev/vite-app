package az.ingress.paymentsapp.controller;

import az.ingress.paymentsapp.dto.request.PaymentRequest;
import az.ingress.paymentsapp.dto.response.PaymentResponse;
import az.ingress.paymentsapp.service.PaymentService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping("/api/v1/payments")
@RequiredArgsConstructor
@SecurityRequirement(name = "bearerAuth")
public class PaymentController {

    private final PaymentService paymentService;

    @GetMapping
    public ResponseEntity<List<PaymentResponse>> findAll() {
        return new ResponseEntity<>(paymentService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/{paymentId}")
    public ResponseEntity<PaymentResponse> findById(@PathVariable Long paymentId) {
        return new ResponseEntity<>(paymentService.findById(paymentId), HttpStatus.OK);
    }


    @PostMapping("/students/{studentId}/courses/{courseId}")
    public ResponseEntity<PaymentResponse> save(@ModelAttribute PaymentRequest request,
                                                        @RequestParam("file") MultipartFile file,
                                                        @PathVariable Long studentId,
                                                        @PathVariable Long courseId) {
        return new ResponseEntity<>(paymentService.save(request, file, studentId, courseId), HttpStatus.OK);
    }

    @PutMapping("/{paymentId}")
    public ResponseEntity<PaymentResponse> update(@RequestBody PaymentRequest request,
                                                  @PathVariable Long paymentId) {
        return new ResponseEntity<>(paymentService.update(paymentId, request), HttpStatus.OK);
    }

    @DeleteMapping("/{paymentId}")
    public void delete(@PathVariable Long paymentId) {
        paymentService.delete(paymentId);
    }
}
