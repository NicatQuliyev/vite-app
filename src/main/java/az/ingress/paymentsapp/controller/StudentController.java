package az.ingress.paymentsapp.controller;

import az.ingress.paymentsapp.dto.request.StudentRequest;
import az.ingress.paymentsapp.dto.response.StudentResponse;
import az.ingress.paymentsapp.service.StudentService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/students")
@RequiredArgsConstructor
@Slf4j
@SecurityRequirement(name = "bearerAuth")
public class StudentController {

    private final StudentService studentService;

    @GetMapping
    public ResponseEntity<List<StudentResponse>> findAll() {
        return new ResponseEntity<>(studentService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/{studentId}")
    public ResponseEntity<StudentResponse> findById(@PathVariable Long studentId) {
        return new ResponseEntity<>(studentService.findById(studentId), HttpStatus.OK);
    }

    @GetMapping("/number/{phoneNumber}")
    public ResponseEntity<StudentResponse> findStudentByPhoneNumber(@PathVariable String phoneNumber) {
        return new ResponseEntity<>(studentService.findByPhoneNumber(phoneNumber), HttpStatus.OK);
    }

    @PostMapping("/numbers/{numberId}")
    public ResponseEntity<StudentResponse> save(@RequestBody StudentRequest request,
                                                @PathVariable Long numberId) {
        return new ResponseEntity<>(studentService.save(request, numberId), HttpStatus.CREATED);
    }

    @PutMapping("/{studentId}/numbers/{numberId}")
    public ResponseEntity<StudentResponse> update(@RequestBody StudentRequest request,
                                                  @PathVariable Long studentId,
                                                  @PathVariable Long numberId) {
        return new ResponseEntity<>(studentService.update(request, studentId, numberId), HttpStatus.OK);
    }

    @DeleteMapping("/{numberId}")
    public void delete(@PathVariable Long numberId) {
        studentService.delete(numberId);
    }
}
