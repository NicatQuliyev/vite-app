package az.ingress.paymentsapp.controller;

import az.ingress.paymentsapp.dto.request.LoginRequest;
import az.ingress.paymentsapp.dto.request.SignUpRequest;
import az.ingress.paymentsapp.dto.response.Response;
import az.ingress.paymentsapp.service.UserService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/auth")
@RequiredArgsConstructor
@Slf4j
public class UserController {

    private final UserService userService;

    @PostMapping("/signup")
    public ResponseEntity<Response> registerUser(@RequestBody @Valid SignUpRequest signUpRequest) {

        return userService.registerUser(signUpRequest);

    }

    @PostMapping("/signin")
    public ResponseEntity<Response> login(@RequestBody @Valid LoginRequest loginRequest) {
        return userService.loginUser(loginRequest);
    }

    @GetMapping("/confirmation")
    public ResponseEntity<?> confirmation(@RequestParam("confirmationToken") String confirmationToken) {
        return userService.confirmation(confirmationToken);
    }
}
