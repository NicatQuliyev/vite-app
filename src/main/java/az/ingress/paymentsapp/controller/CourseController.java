package az.ingress.paymentsapp.controller;

import az.ingress.paymentsapp.dto.request.CourseRequest;
import az.ingress.paymentsapp.dto.request.PhoneNumberRequest;
import az.ingress.paymentsapp.dto.response.CourseResponse;
import az.ingress.paymentsapp.entity.Course;
import az.ingress.paymentsapp.service.CourseService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/courses")
@RequiredArgsConstructor
@SecurityRequirement(name = "bearerAuth")
public class CourseController {

    private final CourseService courseService;

    @GetMapping
    public ResponseEntity<List<CourseResponse>> findAll() {
        return new ResponseEntity<>(courseService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/{courseId}")
    public ResponseEntity<CourseResponse> findById(@PathVariable Long courseId) {
        return new ResponseEntity<>(courseService.findById(courseId), HttpStatus.OK);
    }

    @GetMapping("/courseName")
    public ResponseEntity<CourseResponse> findByCourseName(@RequestParam String name) {
        return new ResponseEntity<>(courseService.findByCourseName(name), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<CourseResponse> save(@RequestBody CourseRequest request) {
        return new ResponseEntity<>(courseService.save(request), HttpStatus.CREATED);
    }

    @PostMapping("/{courseId}/assign")
    public ResponseEntity<Course> assignStudentToCourse(@PathVariable Long courseId,
                                                        @RequestBody PhoneNumberRequest request) {
        return new ResponseEntity<>(courseService.assignStudentToCourse(courseId, request), HttpStatus.CREATED);
    }

    @PutMapping("/{courseId}")
    public ResponseEntity<CourseResponse> update(@PathVariable Long courseId,
                                                 @RequestBody CourseRequest request) {
        return new ResponseEntity<>(courseService.update(courseId, request), HttpStatus.OK);
    }

    @DeleteMapping("/{courseId}")
    public void delete(@PathVariable Long courseId) {
        courseService.delete(courseId);
    }

}
