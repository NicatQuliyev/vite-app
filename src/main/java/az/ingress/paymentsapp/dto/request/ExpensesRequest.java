package az.ingress.paymentsapp.dto.request;

import lombok.*;
import lombok.experimental.FieldDefaults;

import java.time.LocalDate;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ExpensesRequest {

    Double price;

    LocalDate date;

    String priceName;

    String cardHolder;

    String photo;
}
