package az.ingress.paymentsapp.dto.request;

import lombok.*;
import lombok.experimental.FieldDefaults;

import java.time.LocalDate;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PaymentRequest {

    Double price;

    LocalDate checkDate;

    Integer courseMonth;

    String checkPhoto;

    String cardHolder;
}
