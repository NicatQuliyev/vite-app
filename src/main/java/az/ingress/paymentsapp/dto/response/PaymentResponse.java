package az.ingress.paymentsapp.dto.response;

import lombok.*;
import lombok.experimental.FieldDefaults;

import java.time.LocalDate;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PaymentResponse {
    Long id;

    Double price;

    LocalDate checkDate;

    Integer courseMonth;

    String checkPhoto;

    String cardHolder;
}
