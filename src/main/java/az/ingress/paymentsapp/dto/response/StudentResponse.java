package az.ingress.paymentsapp.dto.response;

import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.ArrayList;
import java.util.List;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class StudentResponse {

    Long id;

    String name;

    String surname;

    String email;

    PhoneNumberResponse number;

    List<PaymentResponse> payments = new ArrayList<>();
}
