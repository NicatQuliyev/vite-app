package az.ingress.paymentsapp.dto.response;

import lombok.*;
import lombok.experimental.FieldDefaults;

import java.time.LocalDate;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ExpensesResponse {

    Long id;

    Double price;

    LocalDate date;

    String priceName;

    String cardHolder;

    String photo;
}
