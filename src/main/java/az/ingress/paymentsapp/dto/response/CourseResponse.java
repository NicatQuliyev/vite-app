package az.ingress.paymentsapp.dto.response;

import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.ArrayList;
import java.util.List;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CourseResponse {
    Long id;

    String courseName;

    List<StudentResponse> students = new ArrayList<>();
}
