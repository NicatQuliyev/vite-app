package az.ingress.paymentsapp.repository;

import az.ingress.paymentsapp.entity.PhoneNumber;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PhoneNumberRepository extends JpaRepository<PhoneNumber, Long> {


}
