package az.ingress.paymentsapp.repository;

import az.ingress.paymentsapp.entity.Course;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Collection;
import java.util.Optional;

public interface CourseRepository extends JpaRepository<Course, Long> {

    @Query(value = "select c from Course c join fetch c.students s where s.id = :id")
    Collection<Course> findAllByStudentId(Long id);

    Optional<Course> findByCourseName(String name);
}
