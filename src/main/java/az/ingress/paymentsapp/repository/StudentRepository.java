package az.ingress.paymentsapp.repository;

import az.ingress.paymentsapp.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface StudentRepository extends JpaRepository<Student, Long> {

    Optional<Student> findByNumberPhoneNumber(String phoneNumber);

    Boolean existsByEmail(String email);

    Optional<Student> findByNumber_PhoneNumber(String phoneNumber);


}
