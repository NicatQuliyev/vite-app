package az.ingress.paymentsapp.repository;

import az.ingress.paymentsapp.entity.Expenses;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ExpensesRepository extends JpaRepository<Expenses, Long> {
}
