package az.ingress.paymentsapp.repository;

import az.ingress.paymentsapp.entity.Payment;
import az.ingress.paymentsapp.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface PaymentRepository extends JpaRepository<Payment, Long> {
}
