package az.ingress.paymentsapp.repository;

import az.ingress.paymentsapp.entity.Authority;
import az.ingress.paymentsapp.entity.UserAuthority;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AuthorityRepository extends JpaRepository<Authority, Long> {

    Optional<Authority> findByAuthority(UserAuthority userAuthority);
}
