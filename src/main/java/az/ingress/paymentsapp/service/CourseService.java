package az.ingress.paymentsapp.service;

import az.ingress.paymentsapp.dto.request.CourseRequest;
import az.ingress.paymentsapp.dto.request.PhoneNumberRequest;
import az.ingress.paymentsapp.dto.response.CourseResponse;
import az.ingress.paymentsapp.entity.Course;
import az.ingress.paymentsapp.entity.Student;
import az.ingress.paymentsapp.exception.CourseNotFoundException;
import az.ingress.paymentsapp.exception.ErrorCodes;
import az.ingress.paymentsapp.exception.StudentNotFoundException;
import az.ingress.paymentsapp.exception.UserAlreadyAssignedException;
import az.ingress.paymentsapp.repository.CourseRepository;
import az.ingress.paymentsapp.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CourseService {

    private final CourseRepository courseRepository;

    private final StudentRepository studentRepository;

    private final ModelMapper modelMapper;


    public List<CourseResponse> findAll() {
        return courseRepository
                .findAll()
                .stream()
                .map(course -> modelMapper.map(course, CourseResponse.class))
                .collect(Collectors.toList());
    }

    public CourseResponse findById(Long id) {
        Course course = courseRepository.findById(id).orElseThrow(() ->
                new CourseNotFoundException(ErrorCodes.COURSE_NOT_FOUND));

        return modelMapper.map(course, CourseResponse.class);
    }

    public CourseResponse findByCourseName(String courseName) {
        Course course = courseRepository.findByCourseName(courseName).orElseThrow(() ->
                new CourseNotFoundException(ErrorCodes.COURSE_NOT_FOUND));

        return modelMapper.map(course, CourseResponse.class);
    }

    public CourseResponse save(CourseRequest request) {
        Course course = modelMapper.map(request, Course.class);

        return modelMapper.map(courseRepository.save(course), CourseResponse.class);
    }

    public CourseResponse update(Long id, CourseRequest request) {
        courseRepository.findById(id).orElseThrow(() ->
                new CourseNotFoundException(ErrorCodes.COURSE_NOT_FOUND));


        Course course = modelMapper.map(request, Course.class);
        course.setId(id);

        return modelMapper.map(courseRepository.save(course), CourseResponse.class);
    }

    public void delete(Long id) {
        Course course = courseRepository.findById(id).orElseThrow(() ->
                new CourseNotFoundException(ErrorCodes.COURSE_NOT_FOUND));

        courseRepository.delete(course);
    }

    public Course assignStudentToCourse(Long courseId, PhoneNumberRequest phoneNumber) {
        Course course = courseRepository.findById(courseId).orElseThrow(() ->
                new CourseNotFoundException(ErrorCodes.COURSE_NOT_FOUND));

        Student student = studentRepository.findByNumberPhoneNumber(phoneNumber.getPhoneNumber())
                .orElseThrow(() -> new StudentNotFoundException(ErrorCodes.STUDENT_NOT_FOUND));

        if (course.getStudents().contains(student)) {
            throw new UserAlreadyAssignedException(ErrorCodes.USER_ALREADY_ASSIGNED);
        }

        course.getStudents().add(student);

        return courseRepository.save(course);
    }
}
