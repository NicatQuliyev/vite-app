package az.ingress.paymentsapp.service;

import az.ingress.paymentsapp.dto.request.PhoneNumberRequest;
import az.ingress.paymentsapp.dto.response.PhoneNumberResponse;
import az.ingress.paymentsapp.entity.PhoneNumber;
import az.ingress.paymentsapp.exception.ErrorCodes;
import az.ingress.paymentsapp.exception.PhoneNumberNotFoundException;
import az.ingress.paymentsapp.repository.PhoneNumberRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class PhoneNumberService {

    private final PhoneNumberRepository numberRepository;

    private final ModelMapper modelMapper;

    public List<PhoneNumberResponse> findAll() {
        return numberRepository
                .findAll()
                .stream()
                .map(number -> modelMapper.map(number, PhoneNumberResponse.class))
                .collect(Collectors.toList());
    }

    public PhoneNumberResponse findById(Long numberId) {
        PhoneNumber number = numberRepository.findById(numberId).orElseThrow(() ->
                new PhoneNumberNotFoundException(ErrorCodes.PHONENUMBER_NOT_FOUND));

        return modelMapper.map(number, PhoneNumberResponse.class);
    }
    public PhoneNumberResponse save(PhoneNumberRequest request) {
        PhoneNumber number = modelMapper.map(request, PhoneNumber.class);

        return modelMapper.map(numberRepository.save(number), PhoneNumberResponse.class);
    }
    public PhoneNumberResponse update(PhoneNumberRequest request, Long numberId) {
        numberRepository.findById(numberId).orElseThrow(() ->
                new PhoneNumberNotFoundException(ErrorCodes.PHONENUMBER_NOT_FOUND));

        PhoneNumber number = modelMapper.map(request, PhoneNumber.class);
        number.setId(numberId);

        return modelMapper.map(numberRepository.save(number), PhoneNumberResponse.class);
    }

    public void delete(Long numberId) {
        PhoneNumber number = numberRepository.findById(numberId).orElseThrow(() ->
                new PhoneNumberNotFoundException(ErrorCodes.PHONENUMBER_NOT_FOUND));

        numberRepository.delete(number);
    }
}
