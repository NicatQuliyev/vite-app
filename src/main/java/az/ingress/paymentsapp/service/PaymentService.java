package az.ingress.paymentsapp.service;

import az.ingress.paymentsapp.dto.request.PaymentRequest;
import az.ingress.paymentsapp.dto.response.PaymentResponse;
import az.ingress.paymentsapp.entity.Course;
import az.ingress.paymentsapp.entity.Payment;
import az.ingress.paymentsapp.entity.Student;
import az.ingress.paymentsapp.exception.*;
import az.ingress.paymentsapp.repository.CourseRepository;
import az.ingress.paymentsapp.repository.PaymentRepository;
import az.ingress.paymentsapp.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class PaymentService {

    private final PaymentRepository paymentRepository;

    private final StudentRepository studentRepository;

    private final ModelMapper modelMapper;

    private final CourseRepository courseRepository;

    @Value("${upload.path}")
    private String uploadPath;

    public List<PaymentResponse> findAll() {
        return paymentRepository
                .findAll()
                .stream()
                .map(payment -> modelMapper.map(payment, PaymentResponse.class))
                .collect(Collectors.toList());
    }

    public PaymentResponse findById(Long paymentId) {
        Payment payment = paymentRepository.findById(paymentId).orElseThrow(() ->
                new PaymentNotFoundException(ErrorCodes.PAYMENT_NOT_FOUND));

        return modelMapper.map(payment, PaymentResponse.class);
    }

    public PaymentResponse save(PaymentRequest request, MultipartFile file, Long studentId, Long courseId) {

        Student student = studentRepository.findById(studentId).orElseThrow(() ->
                new StudentNotFoundException(ErrorCodes.STUDENT_NOT_FOUND));

        Course course = courseRepository.findById(courseId).orElseThrow(() ->
                new CourseNotFoundException(ErrorCodes.COURSE_NOT_FOUND));


        Payment payment = modelMapper.map(request, Payment.class);
        payment.setStudent(student);
        payment.setCourse(course);

        String uploadedPhotoName = uploadPhoto(file);
        payment.setCheckPhoto(uploadedPhotoName);

        Payment savedPayment = paymentRepository.save(payment);

        return modelMapper.map(savedPayment, PaymentResponse.class);
    }

    public PaymentResponse update(Long id, PaymentRequest request) {
        Optional<Payment> optionalPayment = paymentRepository.findById(id);

        if (optionalPayment.isPresent()) {
            Payment payment = optionalPayment.get();

            payment.setPrice(request.getPrice());
            payment.setCheckDate(request.getCheckDate());
            payment.setCourseMonth(request.getCourseMonth());
            payment.setCardHolder(request.getCardHolder());

            Payment updatedPayment = paymentRepository.save(payment);
            return modelMapper.map(updatedPayment, PaymentResponse.class);
        } else {

            throw new ExpensesNotFoundException(ErrorCodes.PAYMENT_NOT_FOUND);
        }
    }

    public void delete(Long paymentId) {
        Payment payment = paymentRepository.findById(paymentId).orElseThrow(() ->
                new PaymentNotFoundException(ErrorCodes.PAYMENT_NOT_FOUND));

        paymentRepository.delete(payment);
    }

    public String uploadPhoto(MultipartFile file) {
        try {
            File directory = new File(uploadPath);
            if (!directory.exists()) {
                directory.mkdirs();
            }

            String fileExtension = StringUtils.getFilenameExtension(file.getOriginalFilename());
            String fileName = UUID.randomUUID().toString() + "." + fileExtension;

            Path filePath = Paths.get(uploadPath, fileName);

            Files.write(filePath, file.getBytes());

            return fileName;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }


}
