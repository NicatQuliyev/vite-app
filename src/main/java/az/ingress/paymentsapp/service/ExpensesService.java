package az.ingress.paymentsapp.service;

import az.ingress.paymentsapp.dto.request.ExpensesRequest;
import az.ingress.paymentsapp.dto.response.ExpensesResponse;
import az.ingress.paymentsapp.entity.Expenses;
import az.ingress.paymentsapp.exception.ErrorCodes;
import az.ingress.paymentsapp.exception.ExpensesNotFoundException;
import az.ingress.paymentsapp.repository.ExpensesRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ExpensesService {

    @Value("${upload.path}")
    private String uploadPath;

    private final ExpensesRepository expensesRepository;

    private final ModelMapper modelMapper;


    public List<ExpensesResponse> findAll() {
        return expensesRepository
                .findAll()
                .stream()
                .map(expenses -> modelMapper.map(expenses, ExpensesResponse.class))
                .collect(Collectors.toList());

    }

    public ExpensesResponse findById(Long expensesId) {
        Expenses expenses = expensesRepository.findById(expensesId).orElseThrow(() ->
                new ExpensesNotFoundException(ErrorCodes.EXPENSES_NOT_FOUND));

        return modelMapper.map(expenses, ExpensesResponse.class);
    }


    public ExpensesResponse save(ExpensesRequest request, MultipartFile file) {
        Expenses expenses = modelMapper.map(request, Expenses.class);

        String uploadedPhotoName = uploadPhoto(file);
        expenses.setPhoto(uploadedPhotoName);

        Expenses savedExpenses = expensesRepository.save(expenses);

        return modelMapper.map(savedExpenses, ExpensesResponse.class);
    }

    public ExpensesResponse update(Long id, ExpensesRequest request) {
        Optional<Expenses> optionalExpenses = expensesRepository.findById(id);
        if (optionalExpenses.isPresent()) {
            Expenses expenses = optionalExpenses.get();

            expenses.setPrice(request.getPrice());
            expenses.setDate(request.getDate());
            expenses.setPriceName(request.getPriceName());
            expenses.setCardHolder(request.getCardHolder());

            Expenses updatedExpenses = expensesRepository.save(expenses);
            return modelMapper.map(updatedExpenses, ExpensesResponse.class);
        } else {

            throw new ExpensesNotFoundException(ErrorCodes.EXPENSES_NOT_FOUND);
        }
    }

    public void delete(Long expensesId) {
        Expenses expenses = expensesRepository.findById(expensesId).orElseThrow(() ->
                new ExpensesNotFoundException(ErrorCodes.EXPENSES_NOT_FOUND));

        expensesRepository.delete(expenses);
    }

    public String uploadPhoto(MultipartFile file) {
        try {
            File directory = new File(uploadPath);
            if (!directory.exists()) {
                directory.mkdirs();
            }

            String fileExtension = StringUtils.getFilenameExtension(file.getOriginalFilename());
            String fileName = UUID.randomUUID().toString() + "." + fileExtension;

            Path filePath = Paths.get(uploadPath, fileName);

            Files.write(filePath, file.getBytes());

            return fileName;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

}
