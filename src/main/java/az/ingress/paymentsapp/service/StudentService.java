package az.ingress.paymentsapp.service;

import az.ingress.paymentsapp.dto.request.StudentRequest;
import az.ingress.paymentsapp.dto.response.StudentResponse;
import az.ingress.paymentsapp.entity.PhoneNumber;
import az.ingress.paymentsapp.entity.Student;
import az.ingress.paymentsapp.exception.EmailExistException;
import az.ingress.paymentsapp.exception.ErrorCodes;
import az.ingress.paymentsapp.exception.PhoneNumberNotFoundException;
import az.ingress.paymentsapp.exception.StudentNotFoundException;
import az.ingress.paymentsapp.repository.CourseRepository;
import az.ingress.paymentsapp.repository.PhoneNumberRepository;
import az.ingress.paymentsapp.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class StudentService {

    private final StudentRepository studentRepository;

    private final PhoneNumberRepository numberRepository;

    private final ModelMapper modelMapper;

    private final CourseRepository courseRepository;

    public List<StudentResponse> findAll() {
        return studentRepository
                .findAll()
                .stream()
                .map(student -> modelMapper.map(student, StudentResponse.class))
                .collect(Collectors.toList());
    }

    public StudentResponse findById(Long studentId) {
        Student student = studentRepository.findById(studentId).orElseThrow(() ->
                new StudentNotFoundException(ErrorCodes.STUDENT_NOT_FOUND));

        return modelMapper.map(student, StudentResponse.class);
    }

    public StudentResponse findByPhoneNumber(String phoneNumber) {
        Student student = studentRepository.findByNumber_PhoneNumber(phoneNumber)
                .orElseThrow(() -> new PhoneNumberNotFoundException(ErrorCodes.PHONENUMBER_NOT_FOUND));

        return modelMapper.map(student, StudentResponse.class);
    }

    public StudentResponse save(StudentRequest studentRequest, Long numberId) {

        if (studentRepository.existsByEmail(studentRequest.getEmail())) {
            throw new EmailExistException(ErrorCodes.EMAIL_ALREADY_EXIST);
        }

        PhoneNumber number = numberRepository.findById(numberId).orElseThrow(() ->
                new PhoneNumberNotFoundException(ErrorCodes.PHONENUMBER_NOT_FOUND));

        Student student = modelMapper.map(studentRequest, Student.class);
        student.setNumber(number);

        return modelMapper.map(studentRepository.save(student), StudentResponse.class);
    }

    public StudentResponse update(StudentRequest studentRequest, Long studentId, Long numberId) {
        studentRepository.findById(studentId).orElseThrow(() ->
                new StudentNotFoundException(ErrorCodes.STUDENT_NOT_FOUND));


        PhoneNumber number = numberRepository.findById(numberId).orElseThrow(() ->
                new PhoneNumberNotFoundException(ErrorCodes.PHONENUMBER_NOT_FOUND));

        Student student = modelMapper.map(studentRequest, Student.class);
        student.setId(studentId);
        student.setNumber(number);

        return modelMapper.map(studentRepository.save(student), StudentResponse.class);
    }

    public void delete(Long studentId) {
        var courses = courseRepository.findAllByStudentId(studentId);

        Student student = studentRepository.findById(studentId).orElseThrow(() ->
                new StudentNotFoundException(ErrorCodes.STUDENT_NOT_FOUND));

        courses.forEach(course -> {
            course.getStudents().remove(student);
        });

        courseRepository.saveAll(courses);


        studentRepository.delete(student);
    }

}
