package az.ingress.paymentsapp.entity;

public enum UserAuthority {
    USER,
    ADMIN
}
