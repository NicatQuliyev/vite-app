package az.ingress.paymentsapp.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDate;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "payments")
public class Payment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    Double price;

    LocalDate checkDate;

    Integer courseMonth;

    String checkPhoto;

    String cardHolder;

    @ManyToOne
    @JsonIgnore
    @ToString.Exclude
    @JsonBackReference
    Student student;

    @OneToOne
    @JoinColumn(name = "number_id")
    @JsonIgnore
    @ToString.Exclude
    Course course;
}
