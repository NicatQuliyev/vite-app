package az.ingress.paymentsapp.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "students")
public class Student {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    String name;

    String surname;

    String email;

    @OneToOne
    @JoinColumn(name = "number_id")
    @JsonIgnore
    @ToString.Exclude
    PhoneNumber number;

    @OneToMany(mappedBy = "student", cascade = CascadeType.REMOVE)
    @JsonManagedReference
    List<Payment> payments = new ArrayList<>();
}
