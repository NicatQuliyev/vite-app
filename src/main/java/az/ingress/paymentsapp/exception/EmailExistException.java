package az.ingress.paymentsapp.exception;

import lombok.Getter;

@Getter
public class EmailExistException extends RuntimeException {

    public final ErrorCodes errorCode;

    public EmailExistException(ErrorCodes errorCode) {
        this.errorCode = errorCode;
    }
}
