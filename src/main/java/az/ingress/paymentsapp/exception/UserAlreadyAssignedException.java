package az.ingress.paymentsapp.exception;

import lombok.Getter;

@Getter
public class UserAlreadyAssignedException extends RuntimeException {

    public final ErrorCodes errorCode;


    public UserAlreadyAssignedException(ErrorCodes errorCode) {
        this.errorCode = errorCode;
    }
}
