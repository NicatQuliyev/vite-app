package az.ingress.paymentsapp.exception;

import lombok.Getter;

@Getter
public class CourseNotFoundException extends RuntimeException {

    public final ErrorCodes errorCode;

    public CourseNotFoundException(ErrorCodes errorCode) {
        this.errorCode = errorCode;
    }
}
