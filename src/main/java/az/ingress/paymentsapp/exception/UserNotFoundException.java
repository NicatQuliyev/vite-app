package az.ingress.paymentsapp.exception;

import lombok.Getter;

@Getter
public class UserNotFoundException extends RuntimeException {

    public final ErrorCodes errorCode;


    public UserNotFoundException(ErrorCodes errorCode) {
        this.errorCode = errorCode;
    }
}
