package az.ingress.paymentsapp.exception;

import lombok.Getter;

@Getter
public class PaymentNotFoundException extends RuntimeException {

    public final ErrorCodes errorCode;


    public PaymentNotFoundException(ErrorCodes errorCode) {
        this.errorCode = errorCode;
    }
}
