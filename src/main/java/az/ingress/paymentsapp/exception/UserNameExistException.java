package az.ingress.paymentsapp.exception;

import lombok.Getter;

@Getter
public class UserNameExistException extends RuntimeException {

    public final ErrorCodes errorCode;

    public UserNameExistException(ErrorCodes errorCode) {
        this.errorCode = errorCode;
    }
}
