package az.ingress.paymentsapp.exception;

import lombok.Getter;

@Getter
public class StudentOrNumberNotFoundException extends RuntimeException {

    public final ErrorCodes errorCode;


    public StudentOrNumberNotFoundException(ErrorCodes errorCode) {
        this.errorCode = errorCode;
    }
}