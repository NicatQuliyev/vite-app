package az.ingress.paymentsapp.exception;

import lombok.Getter;

@Getter
public class ExpensesNotFoundException extends RuntimeException {

    public final ErrorCodes errorCode;


    public ExpensesNotFoundException(ErrorCodes errorCode) {
        this.errorCode = errorCode;
    }
}
