package az.ingress.paymentsapp.exception;

import lombok.Getter;

@Getter
public class PhoneNumberNotFoundException extends RuntimeException {

    public final ErrorCodes errorCode;

    public PhoneNumberNotFoundException(ErrorCodes errorCode) {
        this.errorCode = errorCode;
    }
}
