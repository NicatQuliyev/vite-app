package az.ingress.paymentsapp.exception;

import lombok.Getter;

@Getter
public class StudentNotFoundException extends RuntimeException {

    public final ErrorCodes errorCode;


    public StudentNotFoundException(ErrorCodes errorCode) {
        this.errorCode = errorCode;
    }
}
