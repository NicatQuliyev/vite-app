package az.ingress.paymentsapp.service;

import az.ingress.paymentsapp.dto.request.StudentRequest;
import az.ingress.paymentsapp.dto.response.StudentResponse;
import az.ingress.paymentsapp.entity.Course;
import az.ingress.paymentsapp.entity.PhoneNumber;
import az.ingress.paymentsapp.entity.Student;
import az.ingress.paymentsapp.exception.EmailExistException;
import az.ingress.paymentsapp.exception.ErrorCodes;
import az.ingress.paymentsapp.exception.PhoneNumberNotFoundException;
import az.ingress.paymentsapp.exception.StudentNotFoundException;
import az.ingress.paymentsapp.repository.CourseRepository;
import az.ingress.paymentsapp.repository.PhoneNumberRepository;
import az.ingress.paymentsapp.repository.StudentRepository;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.junit.jupiter.api.Test;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class StudentServiceTest {

    @Mock
    private StudentRepository studentRepository;

    @Mock
    private PhoneNumberRepository numberRepository;

    @Mock
    private CourseRepository courseRepository;

    @Mock
    private ModelMapper modelMapper;

    @InjectMocks
    private StudentService studentService;

    @Test
    public void testFindAll() {
        List<Student> students = Arrays.asList(
                new Student(1L, "Nicat", "Quliyev", "quliyevv.nicat2003@gmail.com", null, new ArrayList<>()),
                new Student(2L, "Ali", "Quliyev", "aliquliyev@.com", null, new ArrayList<>())
        );

        List<StudentResponse> expectedResponses = Arrays.asList(
                StudentResponse.builder()
                        .id(1L)
                        .name("Nicat")
                        .surname("Quliyev")
                        .build(),
                StudentResponse.builder()
                        .id(2L)
                        .name("Ali")
                        .surname("Quliyev")
                        .build()
        );

        when(studentRepository.findAll()).thenReturn(students);
        when(modelMapper.map(any(), eq(StudentResponse.class))).thenReturn(expectedResponses.get(0), expectedResponses.get(1));

        List<StudentResponse> actualResponses = studentService.findAll();

        assertEquals(expectedResponses, actualResponses);
    }

    @Test
    public void testFindById_ExistingStudent() {
        Long studentId = 1L;
        Student student = new Student(1L, "Nicat", "Quliyev", "quliyevv.nicat2003@gmail.com", null, new ArrayList<>());
        when(studentRepository.findById(studentId)).thenReturn(Optional.of(student));
        when(modelMapper.map(any(Student.class), eq(StudentResponse.class))).thenReturn(
                StudentResponse.builder()
                        .id(1L)
                        .name("Nicat")
                        .surname("c")
                        .build()
        );

        StudentResponse actualResponse = studentService.findById(studentId);

        assertEquals(studentId, actualResponse.getId());
        assertEquals("Nicat", actualResponse.getName());
        assertEquals("Quliyev", actualResponse.getSurname());
    }

    @Test
    public void testFindById_NonExistingStudent() {
        Long studentId = 1L;
        when(studentRepository.findById(studentId)).thenReturn(Optional.empty());

        assertThrows(StudentNotFoundException.class, () -> studentService.findById(studentId));
    }

    @Test
    public void testFindByPhoneNumber_ExistingStudent() {
        String phoneNumber = "1234567890";
        Student student = new Student(1L, "Nicat", "Quliyev", "quliyevv.nicat2003@gmail.com", null, new ArrayList<>());
        when(studentRepository.findByNumber_PhoneNumber(phoneNumber)).thenReturn(Optional.of(student));
        when(modelMapper.map(any(Student.class), eq(StudentResponse.class))).thenReturn(
                StudentResponse.builder()
                        .id(1L)
                        .name("Nicat")
                        .surname("Quliyev")
                        .build()
        );

        StudentResponse actualResponse = studentService.findByPhoneNumber(phoneNumber);

        assertEquals(1L, actualResponse.getId());
        assertEquals("Nicat", actualResponse.getName());
        assertEquals("Quliyev", actualResponse.getSurname());
    }

    @Test
    public void testFindByPhoneNumber_NonExistingPhoneNumber() {
        String phoneNumber = "1234567890";
        when(studentRepository.findByNumber_PhoneNumber(phoneNumber)).thenReturn(Optional.empty());

        assertThrows(PhoneNumberNotFoundException.class, () -> studentService.findByPhoneNumber(phoneNumber));
    }

    @Test
    public void testSave_UniqueEmailAndExistingPhoneNumber() {
        Long numberId = 1L;
        String email = "quliyevv.nicat2003@gmail.com";
        StudentRequest studentRequest = new StudentRequest("Nicat", "Quliyev", email);
        PhoneNumber number = new PhoneNumber(numberId, "1234567890");
        Student student = new Student(1L, "Nicat", "Quliyev", email, number, new ArrayList<>());

        when(studentRepository.existsByEmail(email)).thenReturn(false);
        when(numberRepository.findById(numberId)).thenReturn(Optional.of(number));
        when(modelMapper.map(studentRequest, Student.class)).thenReturn(student);
        when(studentRepository.save(student)).thenReturn(student);
        when(modelMapper.map(student, StudentResponse.class)).thenReturn(StudentResponse.builder()
                .id(1L)
                .name("Nicat")
                .surname("Quliyev")
                .build());

        StudentResponse actualResponse = studentService.save(studentRequest, numberId);

        assertEquals(1L, actualResponse.getId());
        assertEquals("Nicat", actualResponse.getName());
        assertEquals("Quliyev", actualResponse.getSurname());

        verify(studentRepository, times(1)).existsByEmail(email);
        verify(numberRepository, times(1)).findById(numberId);

        verify(studentRepository, times(1)).save(student);
        verify(modelMapper, times(1)).map(studentRequest, Student.class);
        verify(modelMapper, times(1)).map(student, StudentResponse.class);
    }

    @Test
    public void testSave_DuplicateEmail() {
        Long numberId = 1L;
        String email = "quliyevv.nicat2003@gmail.com";
        StudentRequest studentRequest = new StudentRequest("Nicat", "Quliyev", email);

        when(studentRepository.existsByEmail(email)).thenReturn(true);

        EmailExistException exception = assertThrows(EmailExistException.class,
                () -> studentService.save(studentRequest, numberId));

        verify(studentRepository, times(1)).existsByEmail(email);

        verify(numberRepository, never()).findById(anyLong());
        verify(studentRepository, never()).save(any());
        verify(modelMapper, never()).map(any(StudentRequest.class), any(Class.class));

        assertEquals(ErrorCodes.EMAIL_ALREADY_EXIST, exception.getErrorCode());
    }

    @Test
    public void testUpdate_ExistingStudent() {
        Long studentId = 1L;
        Long numberId = 1L;
        StudentRequest studentRequest = new StudentRequest("Nicat", "Quliyev", "quliyevv.nicat2003@gmail.com");
        PhoneNumber number = new PhoneNumber(numberId, "1234567890");
        Student student = new Student(studentId, "Nicat", "Quliyev", "uliyevv.nicat2003@gmail.com", number, new ArrayList<>());
        when(studentRepository.findById(studentId)).thenReturn(Optional.of(student));
        when(numberRepository.findById(numberId)).thenReturn(Optional.of(number));
        when(modelMapper.map(studentRequest, Student.class)).thenReturn(student);
        when(studentRepository.save(student)).thenReturn(student);
        when(modelMapper.map(student, StudentResponse.class)).thenReturn(StudentResponse.builder()
                .id(studentId)
                .name("Nicat")
                .surname("Quliyev")
                .build());

        StudentResponse actualResponse = studentService.update(studentRequest, studentId, numberId);

        assertEquals(studentId, actualResponse.getId());
        assertEquals("Nicat", actualResponse.getName());
        assertEquals("Quliyev", actualResponse.getSurname());
    }

    @Test
    public void testUpdate_NonExistingStudent() {
        Long studentId = 1L;
        Long numberId = 1L;
        StudentRequest studentRequest = new StudentRequest("Nicat", "Quliyev", "quliyevv.nicat2003@gmail.com");
        PhoneNumber number = new PhoneNumber(numberId, "1234567890");
        when(studentRepository.findById(studentId)).thenReturn(Optional.empty());

        assertThrows(StudentNotFoundException.class, () -> studentService.update(studentRequest, studentId, numberId));
    }

    @Test
    public void testDelete_ExistingStudent() {
        Long studentId = 1L;
        Student student = new Student(studentId, "Nicat", "Quliyev", "quliyevv.nicat2003@gmail.com", null, new ArrayList<>());
        List<Course> courses = Arrays.asList(
                new Course(1L, "Java", new ArrayList<>(Arrays.asList(student))),
                new Course(2L, "JavaScript", new ArrayList<>(Arrays.asList(student)))
        );

        when(studentRepository.findById(studentId)).thenReturn(Optional.of(student));
        when(courseRepository.findAllByStudentId(studentId)).thenReturn(courses);

        studentService.delete(studentId);

        verify(courseRepository, times(1)).saveAll(courses);
        verify(studentRepository, times(1)).delete(student);
    }

    @Test
    public void testDelete_NonExistingStudent() {
        Long studentId = 1L;

        when(studentRepository.findById(studentId)).thenReturn(Optional.empty());

        assertThrows(StudentNotFoundException.class, () -> studentService.delete(studentId));

        verify(studentRepository, times(1)).findById(studentId);

        verify(courseRepository, never()).saveAll(anyList());
        verify(studentRepository, never()).delete(any());
    }

}