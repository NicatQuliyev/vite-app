package az.ingress.paymentsapp.service;

import az.ingress.paymentsapp.dto.request.LoginRequest;
import az.ingress.paymentsapp.dto.request.SignUpRequest;
import az.ingress.paymentsapp.dto.response.Response;
import az.ingress.paymentsapp.entity.Authority;
import az.ingress.paymentsapp.entity.User;
import az.ingress.paymentsapp.entity.UserAuthority;
import az.ingress.paymentsapp.exception.*;
import az.ingress.paymentsapp.repository.AuthorityRepository;
import az.ingress.paymentsapp.repository.UserRepository;
import az.ingress.paymentsapp.security.JwtService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {
    @Mock
    private UserRepository userRepository;

    @Mock
    private AuthorityRepository authorityRepository;

    @Mock
    private BCryptPasswordEncoder passwordEncoder;

    @Mock
    private EmailService emailService;

    @Mock
    private JwtService jwtService;

    @InjectMocks
    private UserService userService;

    @Test
    public void testRegisterUserWhenValidSignUpRequest() {
        // Arrange
        SignUpRequest signUpRequest = new SignUpRequest();
        signUpRequest.setEmail("quliyevv.nicat2003@gmail.com");
        signUpRequest.setUsername("quliyeevn");
        signUpRequest.setPassword("qwerty");

        when(userRepository.existsByEmail(signUpRequest.getEmail())).thenReturn(false);
        when(userRepository.existsByUsername(signUpRequest.getUsername())).thenReturn(false);
        when(authorityRepository.findByAuthority(UserAuthority.ADMIN)).thenReturn(Optional.empty());
        when(authorityRepository.save(any(Authority.class))).thenReturn(new Authority());
        when(passwordEncoder.encode(signUpRequest.getPassword())).thenReturn("$2a$10$xbXQaEb2yKOR/n9YsRxJDewhTvsrEqSGf55WoDg5GT.gQ59zL15vq");
        when(userRepository.save(any(User.class))).thenReturn(new User());
        when(jwtService.issueToken(any(User.class))).thenReturn("token");

        // Act
        ResponseEntity<Response> responseEntity = userService.registerUser(signUpRequest);

        // Assert
        assertNotNull(responseEntity);
        assertEquals(ResponseEntity.ok().build().getStatusCode(), responseEntity.getStatusCode());
        assertNotNull(responseEntity.getBody());
        assertNotNull(responseEntity.getBody().getJwt());
        verify(emailService).sendMail(eq(signUpRequest.getEmail()), anyString(), anyString());
    }

    @Test
    public void testRegisterUserWhenEmailAlreadyExists() {
        // Arrange
        SignUpRequest signUpRequest = new SignUpRequest();
        signUpRequest.setEmail("quliyevv.nicat2003@gmail.com");

        when(userRepository.existsByEmail(signUpRequest.getEmail())).thenReturn(true);

        // Act and Assert
        EmailExistException exception = assertThrows(EmailExistException.class, () -> {
            userService.registerUser(signUpRequest);
        });

        assertEquals(ErrorCodes.EMAIL_ALREADY_EXIST, exception.getErrorCode());
        verify(userRepository, never()).existsByUsername(anyString());
        verify(passwordEncoder, never()).encode(anyString());
        verify(userRepository, never()).save(any(User.class));
        verify(jwtService, never()).issueToken(any(User.class));
        verify(emailService, never()).sendMail(anyString(), anyString(), anyString());
    }

    @Test
    public void testRegisterUserWhenUsernameAlreadyExists() {
        // Arrange
        SignUpRequest signUpRequest = new SignUpRequest();
        signUpRequest.setUsername("quliyeevn");

        when(userRepository.existsByEmail(signUpRequest.getEmail())).thenReturn(false);
        when(userRepository.existsByUsername(signUpRequest.getUsername())).thenReturn(true);

        // Act and Assert
        UserNameExistException exception = assertThrows(UserNameExistException.class, () -> {
            userService.registerUser(signUpRequest);
        });

        assertEquals(ErrorCodes.USERNAME_ALREADY_EXIST, exception.getErrorCode());
        verify(userRepository, never()).existsByEmail(anyString());
        verify(passwordEncoder, never()).encode(anyString());
        verify(userRepository, never()).save(any(User.class));
        verify(jwtService, never()).issueToken(any(User.class));
        verify(emailService, never()).sendMail(anyString(), anyString(), anyString());
    }

    @Test
    public void testLoginUserWhenValidLoginRequest() {
        // Arrange
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setEmail("quliyevv.nicat2003@gmail.com");
        loginRequest.setPassword("qwerty");

        User user = new User();
        user.setPassword("$2a$10$xbXQaEb2yKOR/n9YsRxJDewhTvsrEqSGf55WoDg5GT.gQ59zL15vq");

        when(userRepository.findByEmail(loginRequest.getEmail())).thenReturn(Optional.of(user));
        when(passwordEncoder.matches(loginRequest.getPassword(), user.getPassword())).thenReturn(true);
        when(jwtService.issueToken(user)).thenReturn("token");

        // Act
        ResponseEntity<Response> responseEntity = userService.loginUser(loginRequest);

        // Assert
        assertNotNull(responseEntity);
        assertEquals(ResponseEntity.ok().build().getStatusCode(), responseEntity.getStatusCode());
        assertNotNull(responseEntity.getBody());
        assertNotNull(responseEntity.getBody().getJwt());
    }

    @Test
    public void testLoginUserWhenUserNotFound() {
        // Arrange
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setEmail("quliyevv.nicat2003@gmail.com");

        when(userRepository.findByEmail(loginRequest.getEmail())).thenReturn(Optional.empty());

        // Act and Assert
        UserNotFoundException exception = assertThrows(UserNotFoundException.class, () -> {
            userService.loginUser(loginRequest);
        });

        assertEquals(ErrorCodes.USER_NOT_FOUND, exception.getErrorCode());
        verify(passwordEncoder, never()).matches(anyString(), anyString());
        verify(jwtService, never()).issueToken(any(User.class));
    }

    @Test
    public void testLoginUserWhenInvalidPassword() {
        // Arrange
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setEmail("quliyevv.nicat2003@gmail.com");
        loginRequest.setPassword("qwerty");

        User user = new User();
        user.setPassword("$2a$10$xbXQaEb2yKOR/n9YsRxJDewhTvsrEqSGf55WoDg5GT.gQ59zL15vq");

        when(userRepository.findByEmail(loginRequest.getEmail())).thenReturn(Optional.of(user));
        when(passwordEncoder.matches(loginRequest.getPassword(), user.getPassword())).thenReturn(false);

        // Act and Assert
        InvalidPasswordException exception = assertThrows(InvalidPasswordException.class, () -> {
            userService.loginUser(loginRequest);
        });

        assertEquals(ErrorCodes.INVALID_PASSWORD, exception.getErrorCode());
        verify(jwtService, never()).issueToken(any(User.class));
    }

    @Test
    public void testConfirmationWhenUserPresent() {
        // Arrange
        String confirmationToken = "token";
        User user = new User();

        when(userRepository.findByConfirmationToken(confirmationToken)).thenReturn(Optional.of(user));
        when(userRepository.save(user)).thenReturn(user);

        // Act
        ResponseEntity<?> responseEntity = userService.confirmation(confirmationToken);

        // Assert
        assertNotNull(responseEntity);
        assertEquals(ResponseEntity.ok().build().getStatusCode(), responseEntity.getStatusCode());
        assertEquals("User confirmed successfully", responseEntity.getBody());
    }

    @Test
    public void testConfirmationWhenInvalidConfirmationToken() {
        // Arrange
        String confirmationToken = "invalidToken";

        when(userRepository.findByConfirmationToken(confirmationToken)).thenReturn(Optional.empty());

        // Act
        ResponseEntity<?> responseEntity = userService.confirmation(confirmationToken);

        // Assert
        assertNotNull(responseEntity);
        assertEquals(ResponseEntity.ok().build().getStatusCode(), responseEntity.getStatusCode());
        assertEquals("Confirmation token is invalid", responseEntity.getBody());
    }
}