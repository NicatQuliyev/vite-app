package az.ingress.paymentsapp.service;

import az.ingress.paymentsapp.dto.request.CourseRequest;
import az.ingress.paymentsapp.dto.request.PhoneNumberRequest;
import az.ingress.paymentsapp.dto.response.CourseResponse;
import az.ingress.paymentsapp.entity.Course;
import az.ingress.paymentsapp.entity.Student;
import az.ingress.paymentsapp.exception.CourseNotFoundException;
import az.ingress.paymentsapp.exception.StudentNotFoundException;
import az.ingress.paymentsapp.exception.UserAlreadyAssignedException;
import az.ingress.paymentsapp.repository.CourseRepository;
import az.ingress.paymentsapp.repository.StudentRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class CourseServiceTest {
    @InjectMocks
    private CourseService courseService;
    @Mock
    private CourseRepository courseRepository;
    @Mock
    private StudentRepository studentRepository;
    @Mock
    private ModelMapper modelMapper;


    @BeforeEach
    public void setUp() {
        courseRepository = mock(CourseRepository.class);
        studentRepository = mock(StudentRepository.class);
        modelMapper = mock(ModelMapper.class);
        courseService = new CourseService(courseRepository, studentRepository, modelMapper);
    }

    @Test
    public void testFindAll() {
        // Arrange
        Course course1 = Course.builder()
                .id(1L)
                .courseName("Java")
                .build();

        Course course2 = Course.builder()
                .id(2L)
                .courseName("JavaScript")
                .build();
        List<Course> courses = Arrays.asList(course1, course2);

        when(courseRepository.findAll()).thenReturn(courses);

        CourseResponse courseResponse1 = CourseResponse.builder()
                .id(1L)
                .courseName("Java")
                .build();

        CourseResponse courseResponse2 = CourseResponse.builder()
                .id(2L)
                .courseName("JavaScript")
                .build();
        when(modelMapper.map(course1, CourseResponse.class)).thenReturn(courseResponse1);
        when(modelMapper.map(course2, CourseResponse.class)).thenReturn(courseResponse2);

        // Act
        List<CourseResponse> result = courseService.findAll();

        // Assert
        assertEquals(2, result.size());
        assertEquals(courseResponse1, result.get(0));
        assertEquals(courseResponse2, result.get(1));
    }

    @Test
    public void testFindById() {
        // Arrange
        Long courseId = 1L;

        Course course = Course.builder()
                .id(courseId)
                .courseName("Java")
                .build();

        when(courseRepository.findById(courseId)).thenReturn(Optional.of(course));

        CourseResponse expectedResponse = CourseResponse.builder()
                .id(courseId)
                .courseName("Java")
                .build();
        when(modelMapper.map(course, CourseResponse.class)).thenReturn(expectedResponse);

        // Act
        CourseResponse result = courseService.findById(courseId);

        // Assert
        assertEquals(expectedResponse, result);
    }

    @Test
    public void testFindById_NotFound() {
        // Arrange
        Long courseId = 1L;
        when(courseRepository.findById(courseId)).thenReturn(Optional.empty());

        // Act & Assert
        assertThrows(CourseNotFoundException.class, () -> courseService.findById(courseId));
    }

    @Test
    public void testFindByCourseName() {
        // Arrange
        String courseName = "Java";

        Course course = Course.builder()
                .id(1L)
                .courseName(courseName)
                .build();

        when(courseRepository.findByCourseName(courseName)).thenReturn(Optional.of(course));


        CourseResponse expectedResponse = CourseResponse.builder()
                .id(1L)
                .courseName(courseName)
                .build();
        when(modelMapper.map(course, CourseResponse.class)).thenReturn(expectedResponse);

        // Act
        CourseResponse result = courseService.findByCourseName(courseName);

        // Assert
        assertEquals(expectedResponse, result);
    }

    @Test
    public void testFindByCourseName_NotFound() {
        // Arrange
        String courseName = "Non-existing Course";
        when(courseRepository.findByCourseName(courseName)).thenReturn(Optional.empty());

        // Act & Assert
        assertThrows(CourseNotFoundException.class, () -> courseService.findByCourseName(courseName));
    }

    @Test
    public void testSave() {
        // Arrange
        CourseRequest courseRequest = new CourseRequest("Java");

        Course courseToSave = Course.builder()
                .id(1L)
                .courseName("Java")
                .build();

        Course savedCourse = Course.builder()
                .id(1L)
                .courseName("Java")
                .build();

        when(modelMapper.map(courseRequest, Course.class)).thenReturn(courseToSave);
        when(courseRepository.save(courseToSave)).thenReturn(savedCourse);

        CourseResponse expectedResponse = CourseResponse.builder()
                .id(1L)
                .courseName("Java")
                .build();
        when(modelMapper.map(savedCourse, CourseResponse.class)).thenReturn(expectedResponse);

        // Act
        CourseResponse result = courseService.save(courseRequest);

        // Assert
        assertEquals(expectedResponse, result);
    }

    @Test
    public void testUpdate() {
        // Arrange
        Long courseId = 1L;
        CourseRequest courseRequest = new CourseRequest("JavaScript");

        Course existingCourse = Course.builder()
                .id(1L)
                .courseName("Java")
                .build();

        Course updatedCourse = Course.builder()
                .id(1L)
                .courseName("JavaScript")
                .build();

        when(courseRepository.findById(courseId)).thenReturn(Optional.of(existingCourse));
        when(modelMapper.map(courseRequest, Course.class)).thenReturn(updatedCourse);
        when(courseRepository.save(updatedCourse)).thenReturn(updatedCourse);

        CourseResponse expectedResponse = CourseResponse.builder()
                .id(1L)
                .courseName("Updated Course")
                .build();
        when(modelMapper.map(updatedCourse, CourseResponse.class)).thenReturn(expectedResponse);

        // Act
        CourseResponse result = courseService.update(courseId, courseRequest);

        // Assert
        assertEquals(expectedResponse, result);
    }

    @Test
    public void testUpdate_NotFound() {
        // Arrange
        Long courseId = 1L;
        when(courseRepository.findById(courseId)).thenReturn(Optional.empty());
        CourseRequest courseRequest = new CourseRequest("JavaScript");

        // Act & Assert
        assertThrows(CourseNotFoundException.class, () -> courseService.update(courseId, courseRequest));
    }

    @Test
    public void testDelete() {
        // Arrange
        Long courseId = 1L;
        Course course = Course.builder()
                .id(1L)
                .courseName("Java")
                .build();

        when(courseRepository.findById(courseId)).thenReturn(Optional.of(course));

        // Act
        courseService.delete(courseId);

        // Assert
        verify(courseRepository, times(1)).delete(course);
    }

    @Test
    public void testDelete_NotFound() {
        // Arrange
        Long courseId = 1L;
        when(courseRepository.findById(courseId)).thenReturn(Optional.empty());

        // Act & Assert
        assertThrows(CourseNotFoundException.class, () -> courseService.delete(courseId));
    }

    @Test
    public void testAssignStudentToCourse_Success() {
        Long courseId = 1L;
        String phoneNumber = "1234567890";

        Course course = new Course();
        course.setId(courseId);

        PhoneNumberRequest phoneNumberRequest = new PhoneNumberRequest();
        phoneNumberRequest.setPhoneNumber(phoneNumber);

        Student student = Student.builder().
                id(1L)
                .name("Nicat")
                .surname("Quliyev")
                .email("quliyevv.nicat2003@gmail.com")
                .build();

        when(courseRepository.findById(courseId)).thenReturn(Optional.of(course));
        when(studentRepository.findByNumberPhoneNumber(phoneNumber)).thenReturn(Optional.of(student));
        when(courseRepository.save(course)).thenReturn(course); // Mock the save method

        Course assignedCourse = courseService.assignStudentToCourse(courseId, phoneNumberRequest);

        assertNotNull(assignedCourse);
        assertTrue(assignedCourse.getStudents().contains(student));
    }

    @Test
    public void testAssignStudentToCourse_AlreadyAssigned() {
        Long courseId = 1L;
        String phoneNumber = "1234567890";

        Course course = new Course();
        course.setId(courseId);

        PhoneNumberRequest phoneNumberRequest = new PhoneNumberRequest();
        phoneNumberRequest.setPhoneNumber(phoneNumber);

        Student student = new Student();
        // Set student properties here

        course.getStudents().add(student);

        when(courseRepository.findById(courseId)).thenReturn(Optional.of(course));
        when(studentRepository.findByNumberPhoneNumber(phoneNumber)).thenReturn(Optional.of(student));

        try {
            courseService.assignStudentToCourse(courseId, phoneNumberRequest);
            fail("UserAlreadyAssignedException should have been thrown.");
        } catch (UserAlreadyAssignedException e) {
            // Test passed
        }
    }

    @Test
    public void testAssignStudentToCourse_CourseNotFound() {
        Long courseId = 1L;
        String phoneNumber = "1234567890";

        PhoneNumberRequest phoneNumberRequest = new PhoneNumberRequest();
        phoneNumberRequest.setPhoneNumber(phoneNumber);

        when(courseRepository.findById(courseId)).thenReturn(Optional.empty());

        try {
            courseService.assignStudentToCourse(courseId, phoneNumberRequest);
            fail("CourseNotFoundException should have been thrown.");
        } catch (CourseNotFoundException e) {
            // Test passed
        }
    }

    @Test
    public void testAssignStudentToCourse_StudentNotFound() {
        Long courseId = 1L;
        String phoneNumber = "1234567890";

        Course course = new Course();
        course.setId(courseId);

        PhoneNumberRequest phoneNumberRequest = new PhoneNumberRequest();
        phoneNumberRequest.setPhoneNumber(phoneNumber);

        when(courseRepository.findById(courseId)).thenReturn(Optional.of(course));
        when(studentRepository.findByNumberPhoneNumber(phoneNumber)).thenReturn(Optional.empty());

        try {
            courseService.assignStudentToCourse(courseId, phoneNumberRequest);
            fail("StudentNotFoundException should have been thrown.");
        } catch (StudentNotFoundException e) {
            // Test passed
        }
    }
}